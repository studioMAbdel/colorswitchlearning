﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChangerScript : MonoBehaviour
{
    public Color mauve;
    public Color Pink;
    public Color Yellow;
    public Color Blue;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D hak)
    {
        int r = Random.Range(1, 5);

        switch (r)
        {
            case 1:
                hak.gameObject.GetComponent<BallScript>().SetColor(mauve);
                break;
            case 2:
                hak.gameObject.GetComponent<BallScript>().SetColor(Yellow);
                break;          
            case 3:
                hak.gameObject.GetComponent<BallScript>().SetColor(Pink);
                break;          
            case 4:
                hak.gameObject.GetComponent<BallScript>().SetColor(Blue);
                break;
        }

        Destroy(gameObject);
        Debug.Log("HELLO TRIGGER Enter 2d");

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("HELLO TRIGGER EXIT 2d");
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("HELLO TRIGGER Stay 2d");

    }
}
